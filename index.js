const got = require('./data')

const countAllPeople = (data) => {

    let totalCountOfPeople = 0;

    for( const house in data) {
        for (const group of data[house]) {
            totalCountOfPeople += group.people.length
        }
    }
    
    return (console.log(totalCountOfPeople))
    
}

// countAllPeople(got)


const peopleByHouses = (data) => {

    for( const house in data) {
        for (const group of data[house]) {
          console.log(group.people.length)
        }
    }

}

// peopleByHouses(got);

const everyone = (data) => {

    let namesOfPeople = [];

    for( const house in data) {
        for (const group of data[house]) {
          for (const members of group["people"]) {
            namesOfPeople.push(members.name)          }      
        }
    }
    
    return (console.log(namesOfPeople))

}

// everyone(got);

const nameWithS = (data) => {

    let namesOfPeople = [];

    const regex = /s/i

    for( const house in data) {
        for (const group of data[house]) {
          for (const members of group["people"]) {
            regex.test(members.name) ? namesOfPeople.push(members.name) : null    
           }      
        }
    }
    
    return (console.log(namesOfPeople))

}

// nameWithS(got);


const nameWithA = (data) => {

    let namesOfPeople = [];

    const regex = /a/i

    for( const house in data) {
        for (const group of data[house]) {
          for (const members of group["people"]) {
            regex.test(members.name) ? namesOfPeople.push(members.name) : null    
           }      
        }
    }
    
    return (console.log(namesOfPeople))

}

// nameWithA(got);

const surnameWithS = (data) => {
    let namesOfPeople = [];

    const regex = /^S/

    for( const house in data) {
        for (const group of data[house]) {
          for (const members of group["people"]) {
           let name = (members.name).split(" ")[1]
           regex.test(name) ? namesOfPeople.push(members.name) : null    
           }      
        }
    }
    
    return (console.log(namesOfPeople))

}

// surnameWithS(got)


const surnameWithA = (data) => {
    let namesOfPeople = [];

    const regex = /^A/

    for( const house in data) {
        for (const group of data[house]) {
          for (const members of group["people"]) {
           let name = (members.name).split(" ")[1]
           regex.test(name) ? namesOfPeople.push(members.name) : null    
           }      
        }
    }
    
    return (console.log(namesOfPeople))

}

//  surnameWithA(got)


const peopleNameOfAllHouses = (data) => {

    let namesOfPeople = {};

    for( const house in data) {
        for (const group of data[house]) {
           namesOfPeople[group['name']] = null;
           let totalMembers = [];
          for (const members of group["people"]) {
             totalMembers.push(members.name)
           }      
           namesOfPeople[group["name"]] = totalMembers
        }
    }
    
    return (console.log(namesOfPeople))

}

peopleNameOfAllHouses(got)